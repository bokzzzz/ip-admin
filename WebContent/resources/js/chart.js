
function loadChart(labels,values){

new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [
        {
          label: "",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",
"#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          data: values
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Logged users per hour last 24h'
      },
scales: {
        yAxes: [{
            ticks: {
                stepSize: 1
            }
        }]
    }
    }
 });
}
