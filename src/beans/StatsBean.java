package beans;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.google.gson.Gson;

import dao.UserDAO;
import dao.UserLoginDAO;
import dto.UserLoginDTO;

@ManagedBean(name = "statsBean")
@RequestScoped
public class StatsBean implements Serializable {

	/**
	 * 
	 */
	private static final int NumberOfHour=24;
	private static final long serialVersionUID = 3520602949217960951L;
	private int numberOfUsers;
	private int numberOfLoggedUsers;
	public StatsBean() {
		super();
		SetLabelsForChartFunc();
		setNumberOfHoursFunc();
		UserDAO userDAO=new UserDAO();
		setNumberOfUsers(userDAO.getNumberOfUsers());
		setNumberOfLoggedUsers(userDAO.getNumberOfCurrentLoggedUsers());
	}

	private ArrayList<Integer> numberOfHours = new ArrayList<Integer>();
	private ArrayList<String> labelsForCharts = new ArrayList<String>();


	public String getLabelsForCharts() {
		Gson gson = new Gson();
		return gson.toJson(labelsForCharts);
	}

	public void setLabelsForCharts(ArrayList<String> labelsForCharts) {
		this.labelsForCharts = labelsForCharts;
	}

	public ArrayList<Integer> getNumberOfHours() {
		return numberOfHours;
	}

	public void setNumberOfHours(ArrayList<Integer> numberOfHours) {
		this.numberOfHours = numberOfHours;
	}

	private void setNumberOfHoursFunc() {
		HashMap<Integer, Integer> numberByHoursMap = new HashMap<Integer, Integer>();
		for (UserLoginDTO logins : new UserLoginDAO().getAllUsersLogins(NumberOfHour)) {
			int value = numberByHoursMap.getOrDefault(logins.getHour(), 0);
			value++;
			numberByHoursMap.put(logins.getHour(), value);
		}
		LocalDateTime datetime = LocalDateTime.now();
		for (int i = 0; i < NumberOfHour; i++) {
			numberOfHours.add(numberByHoursMap.getOrDefault(datetime.getHour(), 0));
			datetime = datetime.minusHours(1);
		}

	}

	private void SetLabelsForChartFunc() {
		LocalDateTime datetime = LocalDateTime.now();
		labelsForCharts.add(datetime.toLocalDate() + " " + datetime.getHour() + ":00- now");
		for (int i = 0; i < NumberOfHour-1; i++) {
			labelsForCharts.add(datetime.toLocalDate() + " " + datetime.minusHours(1).getHour() + ":00-"
					+ datetime.getHour() + ":00");
			datetime = datetime.minusHours(1);
		}

	}

	public int getNumberOfUsers() {
		return numberOfUsers;
	}

	public void setNumberOfUsers(int numberOfUsers) {
		this.numberOfUsers = numberOfUsers;
	}

	public int getNumberOfLoggedUsers() {
		return numberOfLoggedUsers;
	}

	public void setNumberOfLoggedUsers(int numberOfLoggedUsers) {
		this.numberOfLoggedUsers = numberOfLoggedUsers;
	}



}
