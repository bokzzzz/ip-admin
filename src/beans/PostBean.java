package beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dao.HelpPostDAO;
import dto.HelpPostDTO;
import dto.UserDTO;

@ManagedBean(name = "postBean")
@SessionScoped
public class PostBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8889238095929684164L;
	
	private List<HelpPostDTO> helpPosts=new LinkedList<HelpPostDTO>();

	public List<HelpPostDTO> getHelpPosts() {
		return helpPosts;
	}

	public PostBean() {
		super();
		helpPosts=new HelpPostDAO().getAllPost();
	}

	public void setHelpPosts(List<HelpPostDTO> helpPosts) {
		this.helpPosts = helpPosts;
	}
	public String changeStatusBlock() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("id")) {
			HelpPostDAO helpPostDAO=new HelpPostDAO();
			Predicate<HelpPostDTO> postById = (postPr) -> postPr.getId() == Integer.parseInt(reqMap.get("id")); 
			Optional<HelpPostDTO> postFind= helpPosts.stream().filter(postById).findFirst();
			Integer newStatus= "YES".equals(postFind.get().getIsBlocked() ) ? 0 : 1; 
			if(helpPostDAO.updateBlockStatusPost(Integer.parseInt(reqMap.get("id")),newStatus)) {
				postFind.get().setIsBlocked(newStatus == 0 ? "NO":"YES");
			}
		}
		return "";
	}
	public String deletePost() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("id")) {
			HelpPostDAO helpPostDAO=new HelpPostDAO();
			Predicate<HelpPostDTO> postById = (postPr) -> postPr.getId() == Integer.parseInt(reqMap.get("id")); 
			Optional<HelpPostDTO> postFind= helpPosts.stream().filter(postById).findFirst();
			if(helpPostDAO.deletePost(postFind.get().getId())) {
				helpPosts.remove(postFind.get());
			}
		}
		return "";
	}
}
