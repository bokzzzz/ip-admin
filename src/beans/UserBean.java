package beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


import dao.UserDAO;
import dto.UserDTO;
import mail.MailSender;

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean implements Serializable {

	private static final long serialVersionUID = 5022669426687458041L;
	private List<UserDTO> users=new LinkedList<UserDTO>();
	public List<UserDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserDTO> users) {
		this.users = users;
		
	}
	public UserBean() {
		super();
		this.users=new UserDAO().getAllUsers();
		
	}
	@PostConstruct
	private void init() {
		this.users=new UserDAO().getAllUsers();
	}

	public String changeStatus() {
		
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("username")) {
			UserDAO userDAO = new UserDAO();
			UserDTO user=userDAO.getUserByUsername(reqMap.get("username"));
			String status=user.getStatus();
			if("Active".equals(status)) {
				user.setStatus("Blocked");
			}
			else {
				user.setStatus("Active");
			}
			userDAO.updateStatus(user);
			Predicate<UserDTO> userByUsername = (userPr) -> userPr.getUsername().equals(user.getUsername()); 
			Optional<UserDTO> userFind= users.stream().filter(userByUsername).findFirst();
			userFind.get().setStatus(user.getStatus());
			return "";
		}
		return null;
	}
	public String resetPassword() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("username")) {
			UserDAO userDAO = new UserDAO();
			UserDTO user=userDAO.getUserByUsername(reqMap.get("username"));
			String resetPassword=getRandPass();
			user.setPassword(resetPassword);
			if(userDAO.resetPassword(user)) {
				MailSender.send("Reset password is: "+resetPassword,"Reset Password",user.getEmail());
				return "";
			}
			
		}
		return null;
	}
	private String getRandPass() {
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
		Random rnd = new Random();
		StringBuilder pwd=new StringBuilder();
		for(int i=0;i<15;i++)
			pwd.append(characters.charAt(rnd.nextInt(characters.length())));
		return pwd.toString();
	}
	
}
