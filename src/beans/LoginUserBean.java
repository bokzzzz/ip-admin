package beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import dao.UserDAO;
import dto.UserDTO;

@ManagedBean(name = "userLoginBean")
@RequestScoped
public class LoginUserBean implements Serializable {

	/**
	 * 
	 */
	private String username;
	private String password;
	 private UIComponent component;
	public LoginUserBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 934826443987942259L;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String login() {
		UserDTO user=new UserDAO().getUserByUsernameAndPassword(username, password);
		if(user != null && "Active".equals(user.getStatus()) && "Admin".equals(user.getRole())) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute("user", user);
			username=null;
			password=null;
			return "index.xhtml?faces-redirect=true";
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(component.getClientId(), new FacesMessage("Username or password are incorrect!"));
		return "";
	}
	public UIComponent getComponent() {
		return component;
	}
	public void setComponent(UIComponent component) {
		this.component = component;
	}
	public String logout() {
		HttpSession session =(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.removeAttribute("user");
		return "login.xhtml?faces-redirect=true";
	}
}
