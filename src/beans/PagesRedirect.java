package beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "pagesRedirect")
@SessionScoped
public class PagesRedirect implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4435824670465440185L;
	
	public String users() {
		return "users.xhtml?faces-redirect=true";
	}
	public String posts() {
		return "posts.xhtml?faces-redirect=true";
	}
	public String home() {
		return "index.xhtml?faces-redirect=true";
	}
}
