package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.HelpPostDTO;

public class HelpPostDAO {
	public List<HelpPostDTO> getAllPost() {
		List<HelpPostDTO> posts  = new LinkedList<HelpPostDTO>();
		
		String sql = "select * from help where isDeleted=0 ";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				HelpPostDTO post=new HelpPostDTO();
				post.setId(resultSet.getInt("id"));
				post.setName(resultSet.getString("name"));
				post.setCategory(new CategoryDAO().getNameOfCategotyById(resultSet.getInt("categoryHelp_id")));
				post.setDescription(resultSet.getString("description"));
				post.setImageUrl(resultSet.getString("imageUrl"));
				post.setLocation(resultSet.getString("location"));
				post.setDatetime(LocalDateTime.parse(resultSet.getString("dateTime"),formatter));
				post.setIsBlocked(resultSet.getInt("isBlocked") == 0? "NO":"YES");
				post.setIsReported(resultSet.getInt("isReported") == 0? "NO":"YES");
				posts.add(post);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return posts;
	}
	public boolean updateBlockStatusPost(Integer id,Integer value) {
		String sql = "update help set isBlocked = ? where id= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, value);
			statement.setInt(2, id);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public boolean deletePost(Integer id) {
		String sql = "update help set isDeleted = 1 where id= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
}
