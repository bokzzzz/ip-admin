package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.CategoryDTO;

public class CategoryDAO {
	
	
	public String getNameOfCategotyById(int id) {
		String result="";
		
		String sql = "select * from categoryhelp where id=?";

		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getString("name");
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
}
