package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.UserLoginDTO;

public class UserLoginDAO {
	public List<UserLoginDTO> getAllUsersLogins(int hourNumber) {
		List<UserLoginDTO> usersLogins  = new LinkedList<UserLoginDTO>();
		
		String sql = "select * from userlogin where date > ? or ( date = ? and hour > ?);";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			LocalDateTime dateTime=LocalDateTime.now().minusHours(hourNumber);
			java.sql.Date sqlDate = java.sql.Date.valueOf(dateTime.toLocalDate());
			statement.setString(1,sqlDate.toString());
			statement.setString(2,sqlDate.toString());
			statement.setInt(3,dateTime.getHour());
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				UserLoginDTO userLogin=new UserLoginDTO();
				userLogin.setIdUser(resultSet.getInt("user_id"));
				userLogin.setHour(resultSet.getInt("hour"));
				userLogin.setDate(resultSet.getDate("date"));
				usersLogins.add(userLogin);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return usersLogins;
	}
}
