package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import connection.ConnectionPool;
import dto.UserDTO;

public class UserDAO {
	public List<UserDTO> getAllUsers() {
		List<UserDTO> users  = new LinkedList<UserDTO>();
		
		String sql = "select * from user where role='Classic'";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				UserDTO user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setStatus(resultSet.getString("status"));
				users.add(user);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return users;
	}
	public UserDTO getUserByUsernameAndPassword(String username,String password) {
		String sql = "select * from user where username=? and password=?";
		UserDTO user = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setStatus(resultSet.getString("status"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	
	
	
	public boolean updateStatus(UserDTO user) {
		String sql = "update user set status= ?  where username= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, user.getStatus());
			statement.setString(2, user.getUsername());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public UserDTO getUserByUsername(String username) {
		String sql = "select * from user where username=?";
		UserDTO user = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user=new UserDTO();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setRegion(resultSet.getString("region"));
				user.setCity(resultSet.getString("city"));
				user.setRole(resultSet.getString("role"));
				user.setStatus(resultSet.getString("status"));
				user.setNotification(resultSet.getString("notification"));
				user.setImage(resultSet.getString("image"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	public boolean resetPassword(UserDTO user) {
		String sql = "update user set password= ?  where username= ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, user.getPassword());
			statement.setString(2, user.getUsername());
			
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	public int getNumberOfUsers() {
		
		String sql = "SELECT count(*) as numberOfUsers FROM user where status='Active' and role='Classic'";
		int result=0;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getInt("numberOfUsers");
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
public int getNumberOfCurrentLoggedUsers() {
		
		String sql = "SELECT count(*) as numberOfCurrentLoggedUsers FROM user where status='Active' and role='Classic' and isLoggedIn=1";
		int result=0;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getInt("numberOfCurrentLoggedUsers");
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
}
