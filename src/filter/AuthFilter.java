package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AuthFilter implements Filter {


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);
		String loginURI = req.getContextPath() + "/login.xhtml";
		String indexURI = req.getContextPath() + "/index.xhtml";
		String adminURI = req.getContextPath() + "/posts.xhtml";
		String usersURI = req.getContextPath() + "/users.xhtml";
		String postsURI = req.getContextPath() + "/posts.xhtml";
		String jsfURI = req.getContextPath() + "/javax.faces.resource/jsf.js.xhtml";
		String resources=req.getContextPath() + "/resources";
		
		if (req.getRequestURI().toString().startsWith(resources) || req.getRequestURI().toString().equals(jsfURI)) {
			chain.doFilter(request, response);
		}else if(loginURI.equals(req.getRequestURI().toString()) && session!=null && session.getAttribute("user") != null) {
			res.sendRedirect(indexURI);
		}else if(loginURI.equals(req.getRequestURI().toString()))
		{
			chain.doFilter(request, response);
		}
		else if(session!=null && session.getAttribute("user") != null) {
			if(indexURI.equals(req.getRequestURI().toString())
					|| adminURI.equals(req.getRequestURI().toString()) 
					|| usersURI.equals(req.getRequestURI().toString()) 
					|| postsURI.equals(req.getRequestURI().toString()) 
					) {
				chain.doFilter(request, response);
			}
			else {
				res.sendRedirect(indexURI);
			}
		}
		else {
			res.sendRedirect(loginURI);
		}
		
	}


}
